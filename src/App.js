import "./App.css";
import Router from "./router/";
import Header from "./components/molecules/header";
import Footer from "./components/molecules/footer";

function App() {
  return (
    <>
      <Header />
      <Router />
      <Footer />
    </>
  );
}

export default App;
