import Link from "../../atoms/link";
import "./index.css";

const links = ["Login", "Register"];

const Header = () => {
  return (
    <div className="header">
      {links.map((actual) => {
        return <Link text={actual} />;
      })}
    </div>
  );
};

export default Header;
