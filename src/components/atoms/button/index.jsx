import styled from "styled-components";

const StyledButton = styled.button`
  border-radius: 3px;
  padding: 5px;
  margin: 5px;
  width: 8vw;
  background: transparent;
  color: black;
  border: 2px solid black;
`;

const Button = () => {
  return <StyledButton>Submit</StyledButton>;
};

export default Button;
