import styled from "styled-components";
import { useHistory } from "react-router-dom";

const StyledLink = styled.a`
  text-decoration: none;
  color: white;
  margin-left: 18px;
  cursor: pointer;
  :hover {
    color: green;
  }
`;

const Link = ({ text }) => {
  const history = useHistory();

  return (
    <StyledLink
      onClick={() => {
        text === "Login" ? history.push("/") : history.push("register");
      }}
    >
      {text}
    </StyledLink>
  );
};

export default Link;
