import styled from "styled-components";

const StyledInput = styled.input`
  width: 15vw;
  height: 7vh;
  padding: 5px;
  margin: 5px;
  background: transparent;
  color: black;
  border: 2px solid black;
  :focus {
    outline: none;
  }
`;

const Input = ({ label }) => {
  return <StyledInput placeholder={label} />;
};

export default Input;
