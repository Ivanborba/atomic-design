import styled from "styled-components";

export const Container = styled.div`
  width: 20%;
  height: 50%%;
  align-items: center;
  padding-top: 15px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  border: 1px solid black;
  border-radius: 5px;
`;
